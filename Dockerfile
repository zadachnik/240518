FROM node:14-alpine
ENV NODE_ENV=production
WORKDIR /app
COPY package*.json ./
RUN npm install --only=production
COPY . .
WORKDIR /app/src
EXPOSE 3000
CMD ["node", "index.js"]
